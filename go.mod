module gitlab.com/zer0main/eventsourcing

go 1.15

require (
	github.com/starius/flock v0.0.0-20170317185950-31e2b263f285 // indirect
	github.com/stretchr/testify v1.7.0
	gitlab.com/NebulousLabs/fastrand v0.0.0-20181126182046-603482d69e40
	gitlab.com/zer0main/filestorage v0.0.0-20210603210652-2ee6eb57d4a3
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5 // indirect
)
