package eventsourcing

import (
	"context"
	"encoding/hex"
	"encoding/json"
	"io"
	"io/ioutil"
	"math"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/NebulousLabs/fastrand"
	"gitlab.com/zer0main/filestorage"
)

const (
	numEvents = 100
)

type EventZero struct {
	Something string `json:"something"`
	Num       int64  `json:"num"`
}

func randZero() *EventZero {
	return &EventZero{
		Something: hex.EncodeToString(fastrand.Bytes(40)),
		Num:       int64(fastrand.Intn(99999)),
	}
}

type EventOne struct {
	Bool *bool  `json:"bool"`
	Name string `json:"name"`
	Data []byte `json:"data"`
}

func randOne() *EventOne {
	b := fastrand.Intn(2) > 0
	return &EventOne{
		Bool: &b,
		Name: hex.EncodeToString(fastrand.Bytes(30)),
		Data: fastrand.Bytes(20),
	}
}

type Event struct {
	Time time.Time  `json:"time"`
	Zero *EventZero `json:"event_zero,omitempty"`
	One  *EventOne  `json:"event_one,omitempty"`

	Useless string `json:"-"`
}

func randomEvent() *Event {
	event := &Event{Time: time.Unix(int64(fastrand.Intn(9999999)), 0)}
	zeroOrOne := fastrand.Intn(2)
	if zeroOrOne < 1 {
		event.Zero = randZero()
	} else {
		event.One = randOne()
	}
	return event
}

type EventRead struct {
	Num int64 `json:"num"`
}

func randRead() *EventRead {
	return &EventRead{Num: int64(fastrand.Intn(99999))}
}

type EventWrite struct {
	Fl   float64 `json:"fl"`
	Data []byte  `json:"data"`
}

func randWrite() *EventWrite {
	return &EventWrite{
		Fl:   math.Sqrt(float64(fastrand.Intn(9999999))),
		Data: fastrand.Bytes(10),
	}
}

type Event2 struct {
	Write *EventWrite `json:"event_write,omitempty"`
	Read  *EventRead  `json:"event_read,omitempty"`
}

func randomEvent2() *Event2 {
	event := &Event2{}
	zeroOrOne := fastrand.Intn(2)
	if zeroOrOne < 1 {
		event.Read = randRead()
	} else {
		event.Write = randWrite()
	}
	return event
}

type MockState struct {
	events []*Event
}

func (ms *MockState) DumpHistory(w io.Writer) error {
	encoder := json.NewEncoder(w)
	for _, event := range ms.events {
		if err := encoder.Encode(event); err != nil {
			return err
		}
	}
	return nil
}
func (ms *MockState) Apply(e *Event) {
	ms.events = append(ms.events, e)
}

type MockState2 struct {
	events []*Event2
}

func (ms *MockState2) DumpHistory(w io.Writer) error {
	encoder := json.NewEncoder(w)
	for _, event := range ms.events {
		if err := encoder.Encode(event); err != nil {
			return err
		}
	}
	return nil
}
func (ms *MockState2) Apply(e *Event2) {
	ms.events = append(ms.events, e)
}

func checkStates(t *testing.T, es *EventSourcer, state, stateClone, state1, stateClone1 StateDumper) {
	err := es.LoadEvents(context.Background())
	require.NoError(t, err)
	require.Equal(t, stateClone, state)
	require.Equal(t, stateClone1, state1)
}

func TestEventSourcer(t *testing.T) {
	ctx := context.Background()

	storDir, err := ioutil.TempDir("", "event-sourcer-test")
	require.NoError(t, err)
	t.Cleanup(func() {
		err = os.RemoveAll(storDir)
		require.NoError(t, err)
	})

	stor := filestorage.NewFileStorage(storDir)

	es, err := New(stor, time.Second)
	require.NoError(t, err)
	t.Cleanup(func() {
		err = es.Close(ctx)
		require.NoError(t, err)
	})

	state0 := &MockState2{}
	stateClone0 := &MockState2{}
	state1 := &MockState{}
	stateClone1 := &MockState{}

	// Test RegisterState.
	applyWithoutArgs := func() {}
	applyTooManyArgs := func(e0 *Event, e1 *Event) {}
	applyTooManyReturnValues := func(e0 *Event) error { return nil }
	applyNonPointerArg := func(e0 Event) {}

	tests := []struct {
		applyFunc interface{}
		err       bool
	}{
		{applyWithoutArgs, true},
		{applyTooManyArgs, true},
		{applyTooManyReturnValues, true},
		{applyNonPointerArg, true},
	}
	for _, tc := range tests {
		err = es.RegisterState(state1, tc.applyFunc, nil)
		if tc.err {
			require.Error(t, err)
		} else {
			require.NoError(t, err)
		}
	}

	// Register normal states.
	err = es.RegisterState(state0, state0.Apply, nil)
	require.NoError(t, err)
	err = es.RegisterState(state1, state1.Apply, []string{"time"})
	require.NoError(t, err)
	// Should not allow to register state with the same event keys twice.
	err = es.RegisterState(state1, state1.Apply, []string{"time"})
	require.Error(t, err)

	err = es.LoadEvents(ctx)
	require.NoError(t, err)

	// Test AppendEvent - LoadEvents.
	for i := 0; i < numEvents; i++ {
		e := randomEvent()
		stateClone1.Apply(e)
		err = es.AppendEvent(e)
		require.NoError(t, err)

		e2 := randomEvent2()
		stateClone0.Apply(e2)
		err = es.AppendEvent(e2)
		require.NoError(t, err)
	}
	err = es.Flush(ctx)
	require.NoError(t, err)
	checkStates(t, es, state0, stateClone0, state1, stateClone1)

	// Test ReplaceEvents.
	stateClone1.events = nil
	state1.events = nil
	stateClone0.events = nil
	state0.events = nil
	for i := 0; i < numEvents; i++ {
		e := randomEvent()
		stateClone1.Apply(e)
		state1.Apply(e)

		e2 := randomEvent2()
		stateClone0.Apply(e2)
		state0.Apply(e2)
	}
	err = es.ReplaceEvents(ctx)
	require.NoError(t, err)
	state0.events = nil
	state1.events = nil
	checkStates(t, es, state0, stateClone0, state1, stateClone1)
}
