package eventsourcing

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"reflect"
	"strings"
	"sync"
	"time"
)

type Storage interface {
	LoadMeta(ctx context.Context, callback func(r io.Reader) error) error
	ReplaceMeta(ctx context.Context, callback func(w io.Writer) error) error
	AppendMeta(ctx context.Context, callback func(w io.Writer) error) error

	Lock(ctx context.Context) error
	Unlock(ctx context.Context) error
}

type stateRecord struct {
	applyFunc reflect.Value
	eventType reflect.Type
}

// EventSourcer allows to share storage of event sourcing models among
// multiple States.
// Each of States must be registered using RegisterState() method.
// EventSourcer will load history and call appropriate Apply() deduced
// from event's type for each event.
type EventSourcer struct {
	storage Storage

	states  []StateDumper
	records map[string]*stateRecord

	eventsMu     sync.Mutex // For in-memory only.
	metaMu       sync.Mutex // For drainEventsQueue (involving IO).
	eventsQueue  []interface{}
	persistDelay time.Duration

	closed bool
}

func New(storage Storage, persistDelay time.Duration) (*EventSourcer, error) {
	ctx := context.Background()
	if err := storage.Lock(ctx); err != nil {
		return nil, fmt.Errorf("failed to lock storage: %w", err)
	}
	return &EventSourcer{
		storage:      storage,
		records:      make(map[string]*stateRecord),
		persistDelay: persistDelay,
	}, nil
}

// RegisterState must be called before EventSourcer object is used.
// RegisterState must not be called concurrently.
// applyFunc is a function with one argument and one return value - an error.
// applyFunc's argument is a pointer to event's struct.
// Event's struct must contain a set of pointers to sub events.
// Only one of sub events can be set in particular Event instance.
// All the other pointers must be nil and omitted in JSON.
// Sub events must have unique json keys among all the registered states.
func (es *EventSourcer) RegisterState(state StateDumper, applyFunc interface{}, ignoreFields []string) error {
	applyVal := reflect.ValueOf(applyFunc)
	applyType := reflect.TypeOf(applyFunc)
	if applyType.NumIn() != 1 {
		return fmt.Errorf("apply func has bad number of arguments %d; need 1", applyType.NumIn())
	}
	if applyType.In(0).Kind() != reflect.Ptr {
		return fmt.Errorf("apply func has non-pointer argument, must be a pointer to event struct")
	}
	argType := applyType.In(0).Elem()
	if applyType.NumOut() != 0 {
		return fmt.Errorf("apply func has bad number of return values %d, need 0", applyType.NumOut())
	}

	sr := &stateRecord{applyFunc: applyVal, eventType: argType}

	ignoreMap := make(map[string]bool)
	for _, field := range ignoreFields {
		ignoreMap[field] = true
	}

	for i := 0; i < argType.NumField(); i++ {
		jsonKey := argType.Field(i).Tag.Get("json")
		_, ignore := ignoreMap[jsonKey]
		if jsonKey == "-" || ignore {
			continue
		}
		// Remove ",omitempty".
		chunks := strings.Split(jsonKey, ",")
		rawJsonKey := chunks[0]
		if _, ok := es.records[rawJsonKey]; ok {
			return fmt.Errorf("sub event key %s is not unique, already registered", rawJsonKey)
		}
		es.records[rawJsonKey] = sr
	}
	es.states = append(es.states, state)
	return nil
}

func (es *EventSourcer) extractAndApplyEvent(msg json.RawMessage) error {
	jsonMap := make(map[string]json.RawMessage)
	if err := json.Unmarshal(msg, &jsonMap); err != nil {
		return fmt.Errorf("failed to parse json to key-value: %w", err)
	}
	var sr *stateRecord
	foundKeys := 0
	for key := range jsonMap {
		if r, ok := es.records[key]; ok {
			sr = r
			foundKeys++
		}
	}
	if foundKeys != 1 {
		return fmt.Errorf("wrong number of keys in JSON encoding of an event: %d, need 1", foundKeys)
	}
	eventVal := reflect.New(sr.eventType)
	if err := json.Unmarshal(msg, eventVal.Interface()); err != nil {
		return fmt.Errorf("failed to parse event: %w", err)
	}
	res := sr.applyFunc.Call([]reflect.Value{eventVal})
	if len(res) != 0 {
		panic(fmt.Sprintf("apply func has too many return values: %d, need 0", len(res)))
	}
	return nil
}

func (es *EventSourcer) LoadEvents(ctx context.Context) error {
	err := es.storage.LoadMeta(ctx, func(r io.Reader) error {
		decoder := json.NewDecoder(r)
		decoder.DisallowUnknownFields()

		for {
			var msg json.RawMessage
			if err := decoder.Decode(&msg); err == io.EOF {
				break
			} else if err != nil {
				return fmt.Errorf("failed to decode event: %w", err)
			}
			if err := es.extractAndApplyEvent(msg); err != nil {
				return err
			}
		}
		return nil
	})
	if err != nil {
		return fmt.Errorf("failed to load history: %w", err)
	}
	return nil
}

// ReplaceEvents rewrites the persistent history with events from current states.
func (es *EventSourcer) ReplaceEvents(ctx context.Context) error {
	if err := es.storage.ReplaceMeta(ctx, func(w io.Writer) error {
		for _, state := range es.states {
			if err := state.DumpHistory(w); err != nil {
				return err
			}
		}
		return nil
	}); err != nil {
		return fmt.Errorf("failed to replace history: %w", err)
	}
	return nil
}

// Flush writes all pending events to hard drive (append to file metadata.json).
func (es *EventSourcer) Flush(ctx context.Context) error {
	es.eventsMu.Lock()
	if es.closed {
		es.eventsMu.Unlock()
		return fmt.Errorf("repo is closed")
	}
	es.eventsMu.Unlock()
	return es.drainEventsQueue(ctx)
}

// Mutex eventsMu must NOT be locked when this function is called by EventSourcer's methods.
func (es *EventSourcer) drainEventsQueue(ctx context.Context) error {
	return DrainEventsQueue(ctx, &es.eventsMu, &es.metaMu, &es.eventsQueue, es.storage)
}

func (es *EventSourcer) AppendEvent(event interface{}) error {
	es.eventsMu.Lock()
	defer es.eventsMu.Unlock()

	if es.closed {
		return fmt.Errorf("repo is closed")
	}

	es.eventsQueue = append(es.eventsQueue, event)
	if len(es.eventsQueue) == 1 {
		time.AfterFunc(es.persistDelay, func() {
			if err := es.drainEventsQueue(context.Background()); err != nil {
				log.Printf("drainEventsQueue failed: %v", err)
			}
		})
	}

	return nil
}

func (es *EventSourcer) Close(ctx context.Context) error {
	es.eventsMu.Lock()
	es.closed = true
	es.eventsMu.Unlock()

	if err := es.drainEventsQueue(ctx); err != nil {
		return fmt.Errorf("drainEventsQueue: %w", err)
	}

	if err := es.storage.Unlock(ctx); err != nil {
		return fmt.Errorf("failed to unlock storage: %w", err)
	}

	return nil
}
