package eventsourcing

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"

	"github.com/stretchr/testify/require"
)

type State interface {
	StateLoader
	StateDumper
}

type StateLoader interface {
	LoadHistory(r io.Reader) error
}

type StateDumper interface {
	DumpHistory(w io.Writer) error
}

func RunTests(t *testing.T, stateGen func() StateLoader, encodeState func(interface{}) interface{}) {
	update := os.Getenv("STATE_UPDATE_WANTS") == "on"

	// Find path to test files.
	_, filename, _, ok := runtime.Caller(1)
	require.True(t, ok)
	pkgDir := filepath.Dir(filename)
	testsDir := filepath.Join(pkgDir, "testdata")
	infos, err := ioutil.ReadDir(testsDir)
	require.NoError(t, err)
	for _, info := range infos {
		if !info.IsDir() {
			continue
		}
		subtestDir := filepath.Join(testsDir, info.Name())
		inputFile := filepath.Join(subtestDir, "input.json")
		wantFile := filepath.Join(subtestDir, "want.json")

		t.Run(info.Name(), func(t *testing.T) {
			input, err := ioutil.ReadFile(inputFile)
			require.NoError(t, err)

			// Dump, Load and compare.
			state := stateGen()

			require.NoError(t, state.LoadHistory(bytes.NewReader(input)))

			if state, ok := state.(StateDumper); ok {
				var dump bytes.Buffer
				require.NoError(t, state.DumpHistory(&dump))
				dumpBytes := dump.Bytes()
				state2 := stateGen().(State)
				require.NoError(t, state2.LoadHistory(bytes.NewReader(dumpBytes)))
				require.Equalf(t, encodeState(state), encodeState(state2), "state is different after dump and restore. Dump: %s", string(dumpBytes))
			}

			if update {
				wantBytes, err := json.MarshalIndent(encodeState(state), "", "  ")
				require.NoError(t, err)
				require.NoError(t, ioutil.WriteFile(wantFile, append(wantBytes, '\n'), 0644))
			} else {
				gotState := encodeState(state)
				wantState := reflect.New(reflect.TypeOf(gotState))
				wantBytes, err := ioutil.ReadFile(wantFile)
				require.NoError(t, err)
				require.NoError(t, json.Unmarshal(wantBytes, wantState.Interface()))
				require.Equal(t, wantState.Elem().Interface(), encodeState(state))
			}
		})
	}
}
