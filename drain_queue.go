package eventsourcing

import (
	"context"
	"encoding/json"
	"io"
	"sync"
)

type StorageAppender interface {
	AppendMeta(ctx context.Context, callback func(w io.Writer) error) error
}

func DrainEventsQueue(ctx context.Context, stateMu, storageMu sync.Locker, eventsQueuePtr *[]interface{}, storage StorageAppender) error {
	storageMu.Lock()
	defer storageMu.Unlock()

	stateMu.Lock()
	eventsQueue := make([]interface{}, len(*eventsQueuePtr))
	copy(eventsQueue, *eventsQueuePtr)
	*eventsQueuePtr = (*eventsQueuePtr)[:0]
	stateMu.Unlock()

	if len(eventsQueue) == 0 {
		return nil
	}

	successes := 0
	err := storage.AppendMeta(ctx, func(w io.Writer) error {
		encoder := json.NewEncoder(w)
		for _, event := range eventsQueue {
			if err := encoder.Encode(event); err != nil {
				return err
			}
			successes++
		}
		return nil
	})

	tail := eventsQueue[successes:]

	if err != nil {
		stateMu.Lock()
		*eventsQueuePtr = append(*eventsQueuePtr, tail...)
		stateMu.Unlock()
		return err
	}

	return nil
}
